from helper.file import File
from helper.stringreplacer import StringReplacer
from os import makedirs, getcwd
from os.path import isdir
from argparse import ArgumentParser
import json

def main():
    print("[Code Previewer] Main Script Started")

    default_params_file = File("config/default_params", ".json")
    if default_params_file.doesExist():
        json_object = json.loads(default_params_file.read())
        parser = ArgumentParser()
        parser.add_argument("-i", "--inputfile", dest="inputFile", help="", default=json_object["inputfile_path"])
        parser.add_argument("-p", "--package", dest="syntaxPackage", help="", default=json_object["syntax_package"])
        args = parser.parse_args()

    keywords = [
        {
            "name": "variable",
            "keys": [
                "const",
                "let",
                "var",
                "import",
                "export"
            ]
        },
        {
            "name": "conditional",
            "keys": [
                "if",
                "else",
                "try",
                "except",
                "catch"
            ]
        },
        {
            "name": "loop",
            "keys": [
                "do",
                "while",
                "for",
                "in",
                "foreach",
                "break"
            ]
        }
    ]

    dot = args.inputFile.rfind(".")
    snippetFile = File(args.inputFile[:dot], args.inputFile[dot:])
    if snippetFile.doesExist():
        data = snippetFile.read()
        for group in keywords:
            for key in group["keys"]:
                newstring = "`}<span className=\"codedata__"+group["name"]+"\">"+key+"</span>{`";
                data = StringReplacer(data, newstring, key)         

        data = "const snippet__transpiled = <span className='codedata'>\n{`"+data
        data = data + "`}\n</span>;\n\nexport default snippet__transpiled;"
        outputFile = File(args.inputFile[:dot]+"__transpiled", args.inputFile[dot:])
        outputFile.write(data)


    print("[Code Previewer] Main Script Ended")


if __name__ == "__main__":
    main()
