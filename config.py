from helper.file import File
from os import makedirs, getcwd
from os.path import isdir
import json

default_content = {
    "inputfile_path": "snippets\\example.js",
    "syntax_package": "javascript"
}


def get_config_folder():
    if not isdir(getcwd() + "\\config"):
        print("[Code Previewer] Create Config Directory")
        makedirs(getcwd() + "\\config")


def update_config(json_object):
    value = input("[Input Prompt] Update Input Filepath (Leave blank to skip)\n[Input --> Default]: "+default_content["inputfile_path"]+"\n[Input --> Current]: "+json_object["inputfile_path"]+"\n[Input --> New]: ")
    if value != "" and value != " ":
        json_object["inputfile_path"] = value
    value = input("[Input Prompt] Update Syntax Package (Leave blank to skip)\n[Input --> Default]:"+default_content["syntax_package"]+"\n[Input --> Current]: "+json_object["syntax_package"]+"\n[Input --> New]: ")
    if value != "" and value != " ":
        json_object["syntax_package"] = value
    return json_object;


def validate_config(json_object):
    print("[Code Previewer] Validating Configuration!")
    dot = json_object["inputfile_path"].find(".")
    input_file = File(json_object["inputfile_path"][:dot], json_object["inputfile_path"][dot:])
    if input_file.doesExist() == False:
        print("[Warning] Input file does not exist. Transpilation may be impossible!")
        return False;
    else: 
        print("[Test] Input file does exist")
    if json_object["syntax_package"] != "javascript":
        print("[Warning] Syntax package does not exist. Transpilation may be impossible!")
        return False;
    else:
        print("[Test] Syntax package does exist")
    print("[Code Previewer] Configuration is valid!")
    return True;


def save_config(json_object, default_params_file):
    with open(default_params_file.name + default_params_file.extension, "w+") as jsonFile:
        print("[Code Previewer] Saving Changes")
        jsonFile.seek(0)
        json.dump(json_object, jsonFile, indent=4, sort_keys=True)
        jsonFile.truncate()


def main():
    print("[Code Previewer] Configuration Script started")
        
    restore_default = input("[Input] Restore to default configuration? [Y/N]")
    get_config_folder()
    default_params_file = File("config/default_params", ".json")
    if default_params_file.doesExist() == False or restore_default.lower() == "y" or restore_default.lower() == "yes":
        save_config(default_content, default_params_file)
    
    if restore_default.lower() != "y" and restore_default.lower() != "yes":
        default_params_file = File("config/default_params", ".json")
        if default_params_file.doesExist():
            json_object = json.loads(default_params_file.read())
            json_object = update_config(json_object)
            if validate_config(json_object):
                save_config(json_object, default_params_file)
            else:
                print("[Code Previewer] Configuration is invalid!")
                print("[Code Previewer] Changes will not be saved!")
    else:
        print("[Code Previewer] Configuration Restored")

    print("[Code Previewer] Configuration Script ended")
            

        
if __name__ == "__main__":
    main()
